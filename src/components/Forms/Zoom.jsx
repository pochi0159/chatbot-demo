import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from 'react-hook-form'
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
}));

const Zoom = props => {

    console.log(props.open)
    const [name, setName] = useState("")
    const [title, setTitle] = useState("")
    const [date, setDate] = useState("")
    const [start, setStart] = useState("")
    const [end, setEnd] = useState("")
    const [content, setContent] = useState("")

    const inputName = (event) => {
        setName(event.target.value)
    }

    const inputTitle = (event) => {
        setTitle(event.target.value)
    }

    const inputDate = (event) => {
        setDate(event.target.value)
    }

    const inputStart = (event) => {
        setStart(event.target.value)
    }

    const inputEnd = (event) => {
        setEnd(event.target.value)
    }

    const inputContent = (event) => {
        setContent(event.target.value)
    }

    const today = new Date().toLocaleString();

    const { register, handleSubmit, errors } = useForm();
    const onSubmit = () => {

        const payload = {
            text: 'Zoom URL発行依頼がありました' +
                'お名前:' + name + '\n' +
                'タイトル:' + title + '\n' +
                '日付:' + date + '\n' +
                '開始時間:' + start + '\n' +
                '終了時間:' + end + '\n' +
                '内容' + content
        }
        
        const url = 'https://hooks.slack.com/services/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'

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(payload)
        }).then(() => {
            alert('送信が完了しました。追って連絡致します！')
            setName("")
            setDate("")
            setContent("")
            return props.handleClose()
        })
    }

    return (
        <Dialog
            open={props.open}
            onClose={props.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">Zoom URL発行依頼フォーム</DialogTitle>
            <DialogContent>
                <label htmlFor="name">{"お名前(必須)"}
                    <div className="ui input">
                        <input
                            id="name"
                            value={name} type={"text"} onChange={inputName}
                            name="name" ref={register({ required: true })}
                        /></div>
                    {errors.name && <div className="error__message">名前を入力してください＞</div>}
                </label>
                <label htmlFor="title">{"タイトル(必須)"}
                    <div className="ui input">
                        <input
                            id="title"
                            value={title} type={"text"} onChange={inputTitle}
                            name="title" ref={register({ required: true })}
                        /></div>
                    {errors.name && <div className="error__message">タイトルを入力してください＞</div>}
                </label>
                <label htmlFor="date">{"日付(必須)"}
                    <form className={props.container} noValidate>
                        <input
                        id="date"
                        type="date"
                        defaultValue={today}
                        className={props.textField}
                        onChange={inputDate}
                        InputLabelProps={{
                        shrink: true,
                        }}
                        name="date"
                        ref={register({ required: true })}
                        />
                    </form>
                    {errors.date && <div className="error__message">日付を入力してください＞</div>}
                </label>
                <label htmlFor="end">{"開始時刻(必須)"}
                    <form className={props.container} noValidate>
                        <input
                        id="start"
                        type="time"
                        defaultValue=""
                        className={props.textField}
                        InputLabelProps={{
                        shrink: true,
                        }}
                        inputProps={{
                        step: 300, // 5 min
                        }}
                        onChange={inputStart}
                        name="start"
                        ref={register({ required: true })}
                        />
                    </form>
                    {errors.start && <div className="error__message">開始時刻を入力してください＞</div>}
                </label>
                <label htmlFor="end">{"終了時刻(必須)"}
                    <form className={props.container} noValidate>
                        <input
                        id="end"
                        type="time"
                        defaultValue=""
                        className={props.textField}
                        InputLabelProps={{
                        shrink: true,
                        }}
                        inputProps={{
                        step: 300, // 5 min
                        }}
                        onChange={inputEnd}
                        name="end"
                        ref={register({ required: true })}
                        />
                    </form>
                    {errors.end && <div className="error__message">終了時刻を入力してください＞</div>}
                </label>
                <label htmlFor="content">{"内容(必須)"}
                    <div className="ui input">
                        <input
                            id="content" name="content"
                            multiline={true} rows={5}
                            value={content} type={"text"} onChange={inputContent}
                            ref={register({ required: true })}
                        />
                    </div>
                    {errors.content && <div className="error__message">内容を入力してください＞</div>}
                </label>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.handleClose} color="primary">
                    キャンセル
                </Button>
                {/* <Button onClick={submitForm} color="primary" autoFocus> */}
                <Button color="primary" autoFocus onClick={handleSubmit(onSubmit)}>
                    送信する
                </Button>
            </DialogActions>
        </Dialog>
    )
}
export default Zoom