import React, { useState }  from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from 'react-hook-form'

const useStyles = makeStyles((theme) => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
  }));

const Wework = props => {

    console.log(props.opening)

    const [name, setName] = useState("")
    const [date, setDate] = useState("")
    const [purpose, setPurpose] = useState("")

        const inputName = (event) => {
            setName(event.target.value)
        }

        const inputDate = (event) => {
            setDate(event.target.value)
        }

        const inputPurpose = (event) => {
            setPurpose(event.target.value)
        }
        
        const today = new Date().toLocaleString();

        const { register, handleSubmit, errors } = useForm();
        const onSubmit = () => {    
                             
            const payload = {
              text: '出社前報告がありました\n' +
                    'お名前:' + name + '\n' +
                    '日時:' + date + '\n' +
                    '出社目的:\n' + purpose
            }  
            
            const url = 'https://hooks.slack.com/services/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'

            fetch(url, {
                method: 'POST',
                body: JSON.stringify(payload)
            }).then(() => {
                alert('送信が完了しました。追って連絡致します！')
                setName("")
                setDate("")
                setPurpose("")
                return props.handleClose()
            })
        }
            
        return(
            <Dialog  
                open={props.opening}
                onClose={props.handleClosing}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
            <DialogTitle id="alert-dialog-title">出社用フォーム</DialogTitle>
            <DialogContent>
                <label htmlFor="name">{"お名前(必須)"}
                <div className="ui input">
                <input
                // label={"お名前(必須)"} multiline={false} rows={1}
                id="name"
                value={name} type={"text"} onChange={inputName}
                name="name" ref={register({ required: true})}
                //register={register} required  
            /></div>
             {errors.name && <div className="error__message">名前を入力してください</div>}
             </label>
             <label htmlFor="date">{"日時(必須)"}      
                <form className={props.container} noValidate>
                <input
                    id="date"
                    // label="日時(必須)"
                    type="datetime-local"
                    defaultValue={today}
                    className={props.textField}
                    onChange={inputDate} 
                    InputLabelProps={{
                    shrink: true,
                    }}
                    name="date"
                    ref={register({ required: true})}
                />
                </form>
                {errors.date && <div className="error__message">日付を入力してください</div>}
                </label>
                <label htmlFor="purpose">{"出社目的(必須)"}
                <div className="ui input">
                <input
                    // label={"出社目的(必須)"} 
                    id="purpose" name="purpose"
                    multiline={true} rows={5}
                    value={purpose} type={"text"} onChange={inputPurpose}
                    ref={register({ required: true})}
                 />
                 </div>
                 {errors.purpose && <div className="error__message">出社目的を入力してください</div>}
                 </label>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.handleClosing} color="primary">
                    キャンセル
                </Button>
                {/* <Button onClick={submitForm} color="primary" autoFocus> */}
                <Button color="primary" autoFocus onClick={handleSubmit(onSubmit)}>
                    送信する
                </Button>
            </DialogActions>
          </Dialog>
        )   
}
export default Wework